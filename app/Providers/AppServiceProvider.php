<?php

namespace App\Providers;

use App\Client;
use App\Discipline;
use App\Report;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Client::creating(function ($client) {
          $client->position = Client::max('position') + 1;
        });

        Discipline::creating(function ($discipline) {
          $discipline->position = Discipline::max('position') + 1;
        });

        Report::creating(function ($report) {
          $report->position = Report::max('position') + 1;
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}

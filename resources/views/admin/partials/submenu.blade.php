<aside class="submenu">
  @if (count(config('admin.submenu')))
    <nav>
      <ul class="modules">
        @foreach(config('admin.submenu') as $module)
          <li class="module">
            <a class="module__heading" href="{{ url(sprintf('admin/%s', $module['slug'])) }}">{{ $module['name'] }}</a>
            @if($module['add_button'])
              <ul>
                <li><a class="module__add" href="{{ url(sprintf('admin/%s', $module['slug'])) }}">Toevoegen</a><li>
              </ul>
            @endif
          </li>
        @endforeach
        <li class="module">
          <ul>
            <li><a class="module__add" href="{{ route('admin::report.index') }}">Reportages</a><li>
          </ul>
        </li>
      </ul>
    </nav>
  @endif
</aside>

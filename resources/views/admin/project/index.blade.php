<div class="content__container">
  <div class="content-trail">
    <a href="{{ route('admin::dashboard') }}">Dashboard</a><span>></span><a href="{{ route('admin::project.index') }}">Projecten</a>
  </div>
  <header class="content__header">
    <h1 class="content__header__heading">Projecten</h1>
    <a href="{{ route('admin::project.create') }}" class="content__header__add">Project toevoegen</a>
  </header>
  <section class="content">
    <table class="content__overview">
      <thead class="overview__header">
        <th>Naam</th>
        <th>Actief</th>
      </thead>
      <tbody class="overview__results">
        @foreach($projects as $project)
          <tr data-id="{{ $project->id }}">
            <td onclick="window.location = '{{ route('admin::project.show', $project->id) }}'">{{ $project->name }}</td>
            <td>
              <form action="{{ route('admin::project.toggleStatus', $project->id) }}" method="POST">
                {{ csrf_field() }}
                <button type="submit" class="result-status">
                  @if ($project->active)
                    <span class="result-status--active">
                  @endif
                </button>
              </form>
            </td>
            <td class="edit-cell"><a href="{{ route('admin::project.edit', $project->id) }}">Aanpassen</a></td>
            <td class="destroy-cell">
              <form action="{{ route('admin::project.destroy', $project->id) }}" method="POST">
                {{ method_field('DELETE') }}
                {{ csrf_field() }}
                <button class="js-warning" type="submit">Verwijderen</button>
              </form>
            </td>
          </tr>
        @endforeach
      </tbody>
    </table>
  </section>
</div>

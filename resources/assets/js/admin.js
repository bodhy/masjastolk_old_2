require('../sass/admin/main.scss');

import 'whatwg-fetch';

if (document.querySelector('.js-sortable')) {
  var token = document.querySelector('meta[name="csrf-token"]').getAttribute('content');

  sortRow('.overview__results', 'tr');
  sortRow('.overview__cover', 'li');

  function sortRow(container, element) {
    if (document.querySelector(container)) {
      var sort = Sortable.create(document.querySelector(container), {
        animation: 150, // ms, animation speed moving items when sorting, `0` — without animation
        // handle: 'tr', // Restricts sort start click/touch to the specified element
        ghostClass: 'sortable-ghost',
        draggable: element, // Specifies which items inside the element should be sortable
        onUpdate: function (evt/**Event*/){
           var sortedList = Array.prototype.slice.call(document.querySelectorAll(container + '.js-sortable ' + element)).map(function(item) {
             return item.dataset.id;
           });

           console.log(sortedList);
           var url = window.location.href;
           var re = /^.*\/([a-z]+)\/?(\d)?/g;
           re = re.exec(url);
           var module = re[1];
           if (re[2]) {
             module += '/' + re[2];
           }
           fetch('/admin/' + module + '/sort', {
             method: 'POST',
             body: JSON.stringify({ sortedList }),
             headers: {
               'Accept': 'application/json',
               'Content-Type': 'application/json',
               'X-CSRF-TOKEN': token
             },
             credentials: 'same-origin'
           })
        }
      });
    }
  }
}

if (document.querySelector('.images__grid')) {
  var disciplineSelect = document.querySelector('.js-discipline');
  var clientSelect = document.querySelector('.js-client');
  var images = document.querySelector('.images__grid');

  window.addEventListener('load', requestImages);
  disciplineSelect.addEventListener('change', requestImages);
  clientSelect.addEventListener('change', requestImages);

  function requestImages() {
    let discipline = disciplineSelect.options[disciplineSelect.options.selectedIndex]
    let client = clientSelect.options[clientSelect.options.selectedIndex];
    if (discipline && client) {
      fetch(`/admin/project/${window.location.href.split('/')[5]}/discipline/${discipline.value}/client/${client.value}/images`, {
        credentials: 'same-origin'
      })
      .then(function (response) {
        return response.json();
      }).then(function(response) {
        while (images.firstChild) {
            images.removeChild(images.firstChild);
        }
        for (let key in response) {
          images.innerHTML += `<option data-img-src="/storage/images/medium/${response[key].filename}" value="${response[key].id}">`;
          // console.log(response[key]);
        }
        $('.images__grid').imagepicker()
      });
    }
  }
}
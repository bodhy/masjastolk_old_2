<div class="content__container">
  <div class="content-trail">
    <a href="{{ route('admin::dashboard') }}">Dashboard</a><span>></span><a href="{{ route('admin::project.index') }}">Disciplines</a><span>></span><a href="{{ route('admin::discipline.edit', $discipline->id) }}">{{ $discipline->name }}</a>
  </div>
  <header class="content__header">
    <h1 class="content__header__heading">{{ $discipline->name }}</h1>
  </header>
  <section class="content">
    @if ($disciplineSelectedImages->count())
      <table class="content__overview">
        <thead class="overview__header">
          <th>Afbeelding</th>
          <th>Naam</th>
          <th>Oriëntatie</th>
          <th>Actief</th>
        </thead>
        <tbody class="overview__results js-sortable">
          @foreach($disciplineSelectedImages as $discipline)
            <tr data-id="{{ $discipline->disciplineSelectedImage->id }}">
              <td style="background: url({{ url('storage/images', $discipline->filename) }}) no-repeat center;"></td>
              <td onclick="window.location = ''">{{ str_limit($discipline->name, 25) }}</td>
              <td>{{ $discipline->orientation }}</td>
              <td>
                <form action="{{ route('admin::discipline.toggleImage', $discipline->id) }}" method="POST">
                  {{ csrf_field() }}
                  <button type="submit" class="result-status">
                    @if ($discipline->disciplineSelectedImage()->count())
                      <span class="result-status--active">
                    @endif
                  </button>
                </form>
              </td>
            </tr>
          @endforeach
        </tbody>
      </table>
    @endif
    @if ($images->count())
      <table class="content__overview">
        <thead class="overview__header">
          <th>Afbeelding</th>
          <th>Naam</th>
          <th>Oriëntatie</th>
          <th>Actief</th>
        </thead>
        <tbody class="overview__results">
          @foreach($images as $image)
            <tr>
              <td style="background: url({{ url('storage/images', $image->filename) }}) no-repeat center;"></td>
              <td onclick="window.location = ''">{{ str_limit($image->name, 25) }}</td>
              <td>{{ $image->orientation }}</td>
              <td>
                <form action="{{ route('admin::discipline.toggleImage', $image->id) }}" method="POST">
                  {{ csrf_field() }}
                  <button type="submit" class="result-status">
                    @if ($image->disciplineSelectedImage()->count())
                      <span class="result-status--active">
                    @endif
                  </button>
                </form>
              </td>
            </tr>
          @endforeach
        </tbody>
      </table>
    @endif
  </section>
</div>

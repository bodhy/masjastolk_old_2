<header class="header">
  <div class="wrapper">
    <h1 class="header__heading">{{ 'Welkom, ' . auth()->user()->name }}</h1>
    <nav>
      <ul>
        <li class="menu__list-item"><a class="menu__list-item__anchor"href="">Dashboard</a>
        <li class="menu__list-item"><a class="menu__list-item__anchor"href="">Instellingen</a>
        <li class="menu__list-item">
          <form action={{ action('Auth\LoginController@logout') }} method="POST">
            {{ csrf_field() }}
            <button type="submit" class="menu__list-item__button">Uitloggen</button>
          </form>
      </ul>
    </nav>
  </div>
</header>

var windowWidth = window.innerWidth;
var showMoreButtons = document.querySelectorAll('.detail__button--show-more');

function isShowMoreNeeded() {
  if (window.innerWidth != windowWidth) {
    windowWidth = window.innerWidth;

    for (let showMoreButton of showMoreButtons) {
      let content = showMoreButton.parentNode.querySelector('p');

      let visibleHeight = content.clientHeight;
      let actualHide = content.scrollHeight - 1;

      if (actualHide > visibleHeight) {
        console.log(`${actualHide} greater than ${visibleHeight}`);
        showMoreButton.style.display = 'block';
      }
    }
  }
}

window.addEventListener('load', isShowMoreNeeded);
window.addEventListener('resize', isShowMoreNeeded);

for (let showMoreButton of showMoreButtons) {
  showMoreButton.addEventListener('click', function () {
    let content = showMoreButton.parentNode.querySelector('p');

    if (content.style.maxHeight == '500px') {
      content.style.maxHeight = `${ parseFloat(getComputedStyle(content).fontSize) * 5 }px`;
    } else {
      content.style.maxHeight = '500px';
    }
    showMoreButton.innerHTML = getShowLinkText(showMoreButton.innerHTML);
  });
}

function getShowLinkText(currentText) {
    var newText = '';

    if (currentText.toUpperCase() === 'Lees meer'.toUpperCase()) {
        newText = 'Lees minder';
    } else {
        newText = 'Lees meer';
    }

    return newText;
}

<div class="content__container">
  <div class="content-trail">
    <a href="{{ route('admin::dashboard') }}">Dashboard</a><span>></span><a href="{{ route('admin::project.index') }}">Projecten</a><span>></span><a href="{{ route('admin::project.edit', $project->id) }}">{{ $project->name }}</a>
  </div>
  <header class="content__header">
    <h1 class="content__header__heading">Afbeeldingen</h1>
    <a href="{{ route('admin::project.image.create', $project->id) }}" class="content__header__add">Afbeelding toevoegen</a>
    <a href="{{ route('admin::project.image.images', $project->id) }}" class="content__header__add">Afbeelding selecteren</a>
  </header>
  <section class="content">
    <table class="content__overview">
      <thead class="overview__header">
        <th>Afbeelding</th>
        <th>Naam</th>
        <th>Oriëntatie</th>
        <th>Cover</th>
      </thead>
      <tbody class="overview__results js-sortable">
        @foreach($project->images()->orderBy('image_project.position')->get() as $image)
          <tr data-id="{{ $image->id }}">
            <td style="background: url({{ url('storage/images', $image->filename) }}) no-repeat center;"></td>
            <td>{{ str_limit($image->name, 25) }}</td>
            <td>{{ $image->orientation }}</td>
            <td>
              <form action="{{ route('admin::project.image.toggleCover', ['project' => $project->id, 'image' => $project->images()->find($image->id)->pivot->id]) }}" method="POST">
                {{ csrf_field() }}
                <button type="submit" class="result-status">
                  @if ($image->pivot->cover)
                    <span class="result-status--active">
                  @endif
                </button>
              </form>
            </td>
            <td class="edit-cell"><a href="{{ route('admin::project.image.edit', ['project' => $project->id, 'image' => $image->id]) }}">Aanpassen</a></td>
            <td class="destroy-cell">
              <form action="{{ route('admin::project.image.destroy', ['project' => $project->id, 'image' => $image->id]) }}" method="POST">
                {{ method_field('DELETE') }}
                {{ csrf_field() }}
                <button class="js-warning" type="submit">Verwijderen</button>
              </form>
            </td>
          </tr>
        @endforeach
      </tbody>
    </table>
  </section>
</div>

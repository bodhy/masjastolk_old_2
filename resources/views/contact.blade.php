@extends('layouts.master')

@section('title', 'Masja Stolk Photography')

@section('preload')
  <link rel="prefetch" href="{{ route('home') }}">
  <link rel="prerender" href="{{ route('home') }}">
@endsection

@section('app')
  <article class="page">
    <h1 class="page__heading">Masja Stolk Fotografie</h1>
    <p class="page__paragraph">Als freelance fotograaf werk ik voor commerciële en non-profit organisaties aan de meest uiteenlopende projecten door het hele land. Portret-, reportage- en reclamefotografie zonder al te veel poeha, of zoals veel klanten het zeggen: de Rotterdamse mentaliteit in Gouda. Soms fotografeer ik in opdracht voor particulieren en heel soms geef ik een workshop. Wil je meer weten over wat ik voor je kan doen, of gewoon vrijblijvend kennismaken? Je bent van harte welkom.</p>
    <p class="page__paragraph">En oh ja, bij geen gehoor, is de kans groot dat ik werk aan een documentaire of reportage in het buitenland en dan kun je mij beter even een berichtje sturen. Tot snel.</p>
    <div class="link-group">
      <ul>
        <li class="link-group__links">
          <ul>
            <li class="link-group__link">Masja Stolk Fotografie
            <li class="link-group__link">Lange Groenendaal 92
            <li class="link-group__link">2801 LV Gouda
            <li class="link-group__link">The Netherlands
            <li class="link-group__link"><a href="tel: 0638779738">Telefoon: 06 38 77 97 38</a>
            <li class="link-group__link"><a href="mailto: info@masjastolk.nl">E-mail: info@masjastolk.nl</a>
            <li class="link-group__button"><a class="social__button social__button--linkedin" href="https://www.linkedin.com/in/masjastolk" target="_blank" rel="noopener noreferrer">LinkedIn</a>
            <li class="link-group__button"><a class="social__button social__button--facebook" href="https://www.facebook.com/masja.stolk" target="_blank" rel="noopener noreferrer">facebook</a>
            <li class="link-group__button"><a class="social__button social__button--instagram" href="https://www.instagram.com/masjastolk" target="_blank" rel="noopener noreferrer">instagram</a>
          </ul>
      </ul>
    </div>
    <div id="map"></div>
    <a class="main__button main__button--close" href="{{ route('home') }}" role="button">Close</a>
  </article>
@endsection

@section('script')
  <script>

    function initMap() {
      var uluru = {lat: 52.010571, lng: 4.707618};
      var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 16,
        center: uluru
      });
      var marker = new google.maps.Marker({
        position: uluru,
        map: map
      });
    }

  </script>
  <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAHome4AHaCpLQ-vBrXfEAsqxD2fzp_BeM&callback=initMap"></script>
  <script src="{{ asset('js/project.bundle.js') }}"></script>
@endsection

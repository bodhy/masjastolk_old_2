var path = require('path');
var webpack = require('webpack');
var ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
  entry: {
    app: './resources/assets/js/app.js',
    project: './resources/assets/js/project.js',
    admin: './resources/assets/js/admin.js',
  },
  output: {
    filename: './js/[name].bundle.js',
    path: './public',
    // publicPath: path.resolve('../../../../')
  },
  module: {
    loaders: [
      {
        test: /\.js$/,
        loader: 'babel',
        include: [ path.resolve(__dirname, 'resources/assets/js')],
        query: { presets: ['es2015'] }
      },
      {
        test: /\.scss$/,
        loader: ExtractTextPlugin.extract('style', 'css!postcss!sass'),
        include: [ path.resolve(__dirname, 'resources/assets/sass')]
      },
      { test: /\.(png|woff|woff2|eot|ttf|svg)$/, loader: 'url?limit=10000&name=/fonts/[name].[ext]', include: [ path.resolve(__dirname, 'public/fonts')]  },
      { test: /\.(svg)$/, loader: 'file?name=/media/icons/[name].[ext]', include: [ path.resolve(__dirname, 'public/media')] }
    ],
  },
  postcss: function () {
    return [
      require('autoprefixer'),
      require('css-mqpacker'),
      require('postcss-focus')
    ];
  },
  plugins: [
    // new webpack.optimize.UglifyJsPlugin({
    //   compress: { warnings: false },
    //   output: { comments: false }
    // }),
    // new webpack.optimize.CommonsChunkPlugin('commons', 'commons.js'),
    new ExtractTextPlugin('./css/[name].bundle.css')
  ],
  // devtool: '#inline-source-map'
}

<?php

return [
  'submenu' => [
    [
      'name' => 'Cover',
      'slug' => 'cover',
      'add_button'  => false
    ],
    [
      'name' => 'Projects',
      'slug' => 'project',
      'add_button' => true
    ],
    [
      'name' => 'Images',
      'slug' => 'image',
      'add_button' => true
    ],
    [
      'name' => 'Klanten',
      'slug' => 'client',
      'add_button' => false
    ],
    [
      'name' => 'Disciplines',
      'slug' => 'discipline',
      'add_button' => false
    ]
  ]
];

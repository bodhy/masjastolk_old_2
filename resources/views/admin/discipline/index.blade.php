<div class="content__container">
  <div class="content-trail">
    <a href="{{ route('admin::dashboard') }}">Dashboard</a><span>></span><a href="{{ route('admin::discipline.index') }}">Disciplines</a>
  </div>
  <header class="content__header">
    <h1 class="content__header__heading">Disciplines</h1>
    <a href="{{ route('admin::discipline.create') }}" class="content__header__add">Toevoegen</a>
  </header>
  <section class="content">
    <table class="content__overview js-sortable">
      <thead class="overview__header">
        <th>Naam</th>
        <th>Actief</th>
      </thead>
      <tbody class="overview__results">
        @foreach($disciplines as $discipline)
          <tr data-id="{{ $discipline->id }}">
            <td onclick="window.location = '{{ route('admin::discipline.show', $discipline->id) }}'">{{ $discipline->name }}</td>
            <td>
              <form action="{{ route('admin::discipline.toggleStatus', $discipline->id) }}" method="POST">
                {{ csrf_field() }}
                <button type="submit" class="result-status">
                  @if ($discipline->active)
                    <span class="result-status--active">
                  @endif
                </button>
              </form>
            </td>
            <td class="edit-cell"><a href="{{ route('admin::discipline.edit', $discipline->id) }}">Aanpassen</a></td>
            <td class="destroy-cell">
              <form action="{{ route('admin::discipline.destroy', $discipline->id) }}" method="POST">
                {{ method_field('DELETE') }}
                {{ csrf_field() }}
                <button class="js-warning" type="submit">Verwijderen</button>
              </form>
            </td>
          </tr>
        @endforeach
      </tbody>
    </table>
  </section>
</div>

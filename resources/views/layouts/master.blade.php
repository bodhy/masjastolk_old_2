<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('title')</title>
    <meta name="format-detection" content="telephone=no">
    @section('preload')
    @show
    @section('stylesheet')
      <link href="{{ asset('css/app.bundle.css') }}" rel="stylesheet">
    @show
  </head>
  <body>
    @yield('app')
    @section('script')
      <script src="{{ asset('js/app.bundle.js') }}"></script>
    @show
  </body>
</html>

<article class="project" data-orientation="{{  $project->getOriginal('orientation') }}">
  <div class="project__image">
    <img src="{{ url('storage/images/medium', $project->filename) }}"
      srcset="{{ url('storage/images/small', $project->filename) }} 375w,
        {{ url('storage/images/medium', $project->filename) }} 480w,
        {{ url('storage/images/large', $project->filename) }} 768w"
      data-src="{{ url('storage/images/medium', $project->filename) }}"
      date-srcset="{{ url('storage/images/small', $project->filename) }} 375w,
        {{ url('storage/images/medium', $project->filename) }} 480w,
        {{ url('storage/images/large', $project->filename) }} 768w"
      alt="{{ $project->name }}"
      class="lazyload"
    >
  </div>
  <div class="project__detail">
    <header class="project__header">
      <p class="project__discipline">{{ $project->discipline->name }}</p>
      <h4 class="project__heading">{{ $project->projects()->first()->name }}</h4>
      <a href="{{ route('project', $project->projects()->first()->slug) }}" class="project__button">Bekijken</a>
    </header>
  </div>
</article>

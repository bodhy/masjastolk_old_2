@extends('layouts.master')

@section('title', 'Masja Stolk Photography')

@section('preload')
  <link rel="prefetch" href="{{ route('home') }}">
  <link rel="prerender" href="{{ route('home') }}">
@endsection

@section('app')
  <article class="page">
    <h1 class="page__heading">Even voorstellen</h1>
    <p class="page__paragraph">Hallo, mijn naam is Masja Stolk en ik ben een gedreven fotograaf uit hartje Gouda. Mijn werk bestaat voornamelijk uit reportage- en reclamefotografie, waarbij ik me heb gespecialiseerd in portretfotografie. Sinds mijn studie aan de Fotovakschool in Rotterdam is mijn werk niet onopgemerkt gebleven. Naast mooi werk voor bedrijven en particulieren ben ik regelmatig op reportage in het buitenland.</p>
    <p class="page__paragraph">Daarnaast behoort mijn werk volgens de GUP redactie tot het beste van Nederland en is gepubliceerd in de NEW Dutch Photography Talent catalogus 2012. Het 436 pagina’s dikke boek waarin jaarlijks de beste 100 opkomende fotografen gepresenteerd worden, is dé stijlbijbel voor art-directors, curatoren, verzamelaars, liefhebbers en collega-fotografen.</p>
    
    <h1 class="page__heading">Klanten</h1>
    <p class="page__paragraph">{{ $clients }}</p>

    <h1 class="page__heading">Exposities</h1>
    <p class="page__paragraph">Suspendisse interdum, erat id cursus facilisis, elit mi efficitur velit, id tincidunt leo elit a eros. Morbi sollicitudin ullamcorper molestie. Phasellus aliquam elit ipsum, at volutpat neque rutrum sit amet. Quisque semper mattis tortor, pharetra rhoncus est.</p>

    <img src="http://masjastolk.dev/storage/images/medium/7-portret-fotograaf-ngo-locatie.jpg" srcset="http://masjastolk.dev/storage/images/small/7-portret-fotograaf-ngo-locatie.jpg 375w,
                  http://masjastolk.dev/storage/images/medium/7-portret-fotograaf-ngo-locatie.jpg 480w,
                  http://masjastolk.dev/storage/images/large/7-portret-fotograaf-ngo-locatie.jpg 768w" data-src="http://masjastolk.dev/storage/images/medium/7-portret-fotograaf-ngo-locatie.jpg" date-srcset="http://masjastolk.dev/storage/images/small/7-portret-fotograaf-ngo-locatie.jpg 375w,
                  http://masjastolk.dev/storage/images/medium/7-portret-fotograaf-ngo-locatie.jpg 480w,
                  http://masjastolk.dev/storage/images/large/7-portret-fotograaf-ngo-locatie.jpg 768w" alt="Fotoreportage Water for Life" class="lazyload">

    <table class="reports__table">
      <thead>
        <tr>
          <th>Jaar</th>
          <th>Titel</th>
          <th>Locatie</th>
          <th>Plaats</th>
        </tr>
      </thead>
        <tr><td>2014</td><td>Van de wereld</td><td>UMC Ziekenhuis</td><td>Groningen</td></tr>
        <tr><td>2014</td><td>Van de wereld</td><td>UMC Ziekenhuis</td><td>Amsterdan</td></tr>
        <tr><td>2014</td><td>Van de wereld</td><td>Zuiderkerk</td><td>Amsterdan</td></tr>
        <tr><td>2013</td><td>Eye witness</td><td>The Salon</td><td>Rotterdam</td></tr>
        <tr><td>2013</td><td>Wereld delen</td><td>Blomatelier</td><td>Gouda</td></tr>
        <tr><td>2012</td><td>NEW</td><td>Cosmo Hairstyling</td><td>Gouda</td></tr>
        <tr><td>2012</td><td>Children Underground</td><td>Nieuwe Kerk</td><td>Groningen</td></tr>
        <tr><td>2012</td><td>Children Underground</td><td>Oranjekerk</td><td>Amsterdam</td></tr>
        <tr><td>2012</td><td>Kunstmoment</td><td>Wereldwinkel</td><td>Gouda</td></tr>
        <tr><td>2012</td><td>Children Underground</td><td>Cosmo Hairstyling</td><td>Gouda</td></tr>
        <tr><td>2011</td><td>Little Miss Sunshine</td><td>Fotofestival aan de Maas</td><td>Rotterdam</td></tr>
        <tr><td>2011</td><td>Life in Manikganj</td><td>The Hub</td><td>Rotterdam</td></tr>
        <tr><td>2011</td><td>So far</td><td>Orde van Vrijmetselaren</td><td>Den Haag</td></tr>
        <tr><td>2011</td><td>Contrast</td><td>Ipse de Bruggen</td><td>Delft</td></tr>
        <tr><td>2011</td><td>Beautiful Bangladesh</td><td>Cosmo Hairstyling</td><td>Gouda</td></tr>
        <tr><td>2010</td><td>RAW</td><td>Wereldfestival</td><td>Gouda</td></tr>
        <tr><td>2010</td><td>RAW</td><td>Cosmo Hairstyling</td><td>Gouda</td></tr>
      <tbody>
      </tbody>
    </table>

    <a class="main__button main__button--close" href="{{ route('home') }}" role="button">Close</a>
  </article>
@endsection

@section('script')
  <script src="{{ asset('js/project.bundle.js') }}"></script>
@endsection

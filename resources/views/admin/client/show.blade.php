<div class="content__container">
  <div class="content-trail">
    <a href="{{ route('admin::dashboard') }}">Dashboard</a><span>></span><a href="{{ route('admin::project.index') }}">Klanten</a><span>></span><a href="{{ route('admin::client.edit', $client->id) }}">{{ $client->name }}</a>
  </div>
  <header class="content__header">
    <h1 class="content__header__heading">{{ $client->name }}</h1>
  </header>
  <section class="content">
    @if ($clientSelectedImages->count())
      <table class="content__overview">
        <thead class="overview__header">
          <th>Afbeelding</th>
          <th>Naam</th>
          <th>Oriëntatie</th>
          <th>Actief</th>
        </thead>
        <tbody class="overview__results js-sortable">
          @foreach($clientSelectedImages as $image)
            <tr data-id="{{ $image->clientSelectedImage->id }}">
              <td style="background: url({{ url('storage/images', $image->filename) }}) no-repeat center;"></td>
              <td onclick="window.location = ''">{{ str_limit($image->name, 25) }}</td>
              <td>{{ $image->orientation }}</td>
              <td>
                <form action="{{ route('admin::client.toggleImage', $image->id) }}" method="POST">
                  {{ csrf_field() }}
                  <button type="submit" class="result-status">
                    @if ($image->clientSelectedImage()->count())
                      <span class="result-status--active">
                    @endif
                  </button>
                </form>
              </td>
            </tr>
          @endforeach
        </tbody>
      </table>
    @endif
    @if ($images->count())
      <table class="content__overview">
        <thead class="overview__header">
          <th>Afbeelding</th>
          <th>Naam</th>
          <th>Oriëntatie</th>
          <th>Actief</th>
        </thead>
        <tbody class="overview__results">
          @foreach($images as $image)
            <tr>
              <td style="background: url({{ url('storage/images', $image->filename) }}) no-repeat center;"></td>
              <td onclick="window.location = ''">{{ str_limit($image->name, 25) }}</td>
              <td>{{ $image->orientation }}</td>
              <td>
                <form action="{{ route('admin::client.toggleImage', $image->id) }}" method="POST">
                  {{ csrf_field() }}
                  <button type="submit" class="result-status">
                    @if ($image->clientSelectedImage()->count())
                      <span class="result-status--active">
                    @endif
                  </button>
                </form>
              </td>
            </tr>
          @endforeach
        </tbody>
      </table>
    @endif
  </section>
</div>

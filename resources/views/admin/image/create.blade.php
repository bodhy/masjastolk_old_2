<div class="content__container">
  <div class="content-trail">
    <a href="{{ route('admin::dashboard') }}">Dashboard</a><span>></span><a href="{{ route('admin::project.index') }}">Projecten</a><span>></span><a href="{{ route('admin::project.edit', $project->id) }}">{{ $project->name }}</a>
  </div>
  <header class="content__header">
    <h1 class="content__header__heading">Projecten</h1>
  </header>
  <section class="content">
    <form action="{{ route('admin::project.image.store', $project->id) }}" method="POST" enctype="multipart/form-data">
      {{ csrf_field() }}
      <div class="input__container">
        <label for="name" class="input__label">Naam</label>
        <input type="input" name="name" value="{{ old('name') }}" placeholder="Bijv. Superman" class="input__textfield" id="name">
      </div>
      <div class="input__container">
        <label for="discipline_id" class="input__label">Discipline</label>
        <select class="input__select" name="discipline_id">
          @forelse($disciplines as $discipline)
              <option value="{{ $discipline->id }}" @if(old('discipline_id') && old('discipline') == $discipline->id || $project->discipline->id == $discipline->id) selected @endif>{{ $discipline->name }}</option>
          @empty
            <option value="">Geen disciplines</option>
          @endforelse
        </select>
      </div>
      <div class="input__container">
        <label for="client_id" class="input__label">Klant</label>
        <select class="input__select" name="client_id">
          @forelse($clients as $client)
              <option value="{{ $client->id }}" @if(old('client_id') && old('client') == $client->id) selected @endif>{{ $client->name }}</option>
          @empty
            <option value="">Geen disciplines</option>
          @endforelse
        </select>
      </div>
      <div class="input__container">
        <label for="images" class="input__label">Afbeeldingen</label>
        <input type="file" name="images[]" class="input__file" id="images" multiple>
      </div>
      <div class="input__controls">
        <button type="reset" class="input__button--abort">Annuleren</button>
        <button type="submit" class="input__button--submit">Toevoegen</button>
      </div>
    </form>
    @include('admin.partials.notification')
  </section>
</div>

<?php

namespace App\Http\Controllers;

use App\Project;
use App\Client;
use App\Discipline;
use App\Report;

use Illuminate\Http\Request;

class HomeController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(Project $project, Client $client, Discipline $discipline, Report $report)
    {
      $this->project = $project;
      $this->client = $client;
      $this->discipline = $discipline;
      $this->report = $report;
    }

    /**
     * Show the home page.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $projects = Project::where('active', true)->get();

      $projects = $projects->map(function($item, $key) {
          return $item->images()->where('cover', true)->get();
      })->reject(function($item) {
          return !$item->count();
      })->collapse()->sortBy(function($item) {
        return $item->pivot->cover_position;
      });

      $lastProjects = collect();
      foreach ($projects->reverse() as $key => $project) {
          if (!$project->getOriginal('orientation')) {
              unset($projects[$key]);
              $lastProjects->push($project);
          }
          if (count($lastProjects) >= 5) {
              break;
          }
      }
    
      $lastProjects = $lastProjects->sortBy(function ($item) {
          return $item->pivot->cover_position;
      });

      $clients = Client::where('active', true)->orderBy('position')->get();
      $disciplines = Discipline::where('active', true)->orderBy('position')->get();

      return view('home', compact('projects', 'lastProjects', 'clients', 'disciplines'));
    }

    public function project($project)
    {
      $project = $this->project->where('slug', $project)->first();

      $images = $project->sortedImages->values();
      $images = $this->sortImages($images);

      return view('detail', compact('project', 'images'));

    }

    public function client($client) {
      $client = $this->client->where('slug', $client)->first();

      $images = $client->images()->has('clientSelectedImage')->get();

      $images = $images->filter(function($item) {
        return $item->clientSelectedImage;
      })->sortBy(function ($item) {
        return $item->clientSelectedImage->position;
      })->values();

      $images = $this->sortImages($images);
      $item = $client;

      return view('footer-items', compact('item', 'images'));
    }


    public function discipline($discipline) {
      $discipline = $this->discipline->where('slug', $discipline)->first();

      $images = $discipline->images()->has('disciplineSelectedImage')->get();

      $images = $images->filter(function($item) {
        return $item->disciplineSelectedImage;
      })->sortBy(function ($item) {
        return $item->disciplineSelectedImage->position;
      })->values();

      $images = $this->sortImages($images);
      $item = $discipline;

      return view('footer-items', compact('item', 'images'));
    }

    public function reports() {
      $reports = $this->report->where('active', true)->orderBy('position')->get();
      return view('reports', compact('reports'));
    }

    public function contact()
    {
      return view('contact');
    }

    public function blog()
    {
        return view('blog');
    }

    public function biography()
    {
      $clients = Client::where('active', true)->orderBy('position')->get()->implode('name', ' / ');
      return view('biography', compact('clients'));
    }

    public function top10() 
    {
        return view('top-10');
    }

    private function sortImages($images) {
      $landscape = [];
      $portrait = [];

      $placeholder = [];

      $count = 1;

      foreach ($images as $key => $image) {
          $key++;
          if ($image->getOriginal('orientation')) {
              if (count($placeholder)) {
                  end($placeholder);
                  $placeholder[key($placeholder) + 1] = $image;
                  reset($placeholder);
              } else {
                  $placeholder[$key] = $image;
              }
          }

          if (!$image->getOriginal('orientation')) {
              if (count($placeholder) === 1) {
                  $count++;
                  end($placeholder);
                  $portrait[key($placeholder) + $count] = $image;
                  reset($placeholder);
              } else {
                  $portrait[$key] = $image;
              }
          }

          if (count($placeholder) === 2) {
              $landscape[] = $placeholder;
              $placeholder = [];
              $count = 1;
          }

      }


      $images = [];

      foreach ($landscape as $image) {
          $images[key($image)] = $image;
      }

      foreach ($portrait as $key => $image) {
          $images[$key] = $image;
      }

      ksort($images);

      return $images;
    }
}

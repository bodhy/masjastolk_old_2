<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImageClient extends Model
{
  protected $table = 'image_client';
  protected $fillable = ['image_id', 'position'];
  public $timestamps = false;




  public function clientSelectedImage()
  {
    return $this->belongsTo(Client::class);
  }
}

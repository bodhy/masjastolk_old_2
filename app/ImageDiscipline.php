<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImageDiscipline extends Model
{
  protected $table = 'image_discipline';
  protected $fillable = ['image_id', 'position'];
  public $timestamps = false;

  public function disciplineSelectedImage()
  {
    return $this->belongsTo(Discipline::class);
  }
}

@extends('layouts.master')

@section('stylesheet')
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <link href="{{ asset('css/admin.bundle.css') }}" rel="stylesheet">
@endsection

@section('app')
  @include('admin.partials.menu')
  <section class="main">
    @include('admin.partials.submenu')

    @if (request()->segment(2) && request()->segment(4) && request()->segment(5))
      @include(sprintf('admin.%s.%s', request()->segment(4), request()->segment(5)))
    @elseif (request()->segment(3) || explode('@', Route::getCurrentRoute()->getActionName())[1] === 'index')
      @include(sprintf('%s.%s.%s', request()->segment(1), request()->segment(2), explode('@', Route::getCurrentRoute()->getActionName())[1]))
    @else
      @include(sprintf('%s.%s', request()->segment(1), request()->segment(2)))
    @endif
  </section>
@endsection


@section('script')
  @if (true)
    <script src="//cdnjs.cloudflare.com/ajax/libs/Sortable/1.4.2/Sortable.min.js"></script>
  @endif
  <script src="{{ asset('js/admin.bundle.js') }}"></script>
@endsection

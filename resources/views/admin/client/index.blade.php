<div class="content__container">
  <div class="content-trail">
    <a href="{{ route('admin::dashboard') }}">Dashboard</a><span>></span><a href="{{ route('admin::client.index') }}">Klanten</a>
  </div>
  <header class="content__header">
    <h1 class="content__header__heading">Klanten</h1>
    <a href="{{ route('admin::client.create') }}" class="content__header__add">Toevoegen</a>
  </header>
  <section class="content">
    <table class="content__overview js-sortable">
      <thead class="overview__header">
        <th>Naam</th>
        <th>Actief</th>
      </thead>
      <tbody class="overview__results">
        @foreach($clients as $client)
          <tr data-id="{{ $client->id }}">
            <td onclick="window.location = '{{ route('admin::client.show', $client->id) }}'">{{ $client->name }}</td>
            <td>
              <form action="{{ route('admin::client.toggleStatus', $client->id) }}" method="POST">
                {{ csrf_field() }}
                <button type="submit" class="result-status">
                  @if ($client->active)
                    <span class="result-status--active">
                  @endif
                </button>
              </form>
            </td>
            <td class="edit-cell"><a href="{{ route('admin::client.edit', $client->id) }}">Aanpassen</a></td>
            <td class="destroy-cell">
              <form action="{{ route('admin::client.destroy', $client->id) }}" method="POST">
                {{ method_field('DELETE') }}
                {{ csrf_field() }}
                <button class="js-warning" type="submit">Verwijderen</button>
              </form>
            </td>
          </tr>
        @endforeach
      </tbody>
    </table>
  </section>
</div>

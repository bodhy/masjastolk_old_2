<?php

require 'recipe/laravel.php';

server('prod', 'masjastolk.nl')
    ->user('name')
    ->forwardAgent()
    ->stage('production')
    ->env('deploy_path', 'domains/masjastolk.nl/');

set('repository', 'git@gitlab.com:bodhy/masjastolk.git');

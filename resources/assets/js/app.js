require('../sass/main.scss');
require('es6-promise').polyfill();

import Picturefill from 'picturefill';
import Lazysizes from 'lazysizes';
import ImagesLoaded from 'imagesloaded';
import Masonry from 'masonry-layout';
import FontFaceObserver from 'font-face-observer';

const openMyriadProLightObserver = new FontFaceObserver('MyriadPro-Light', {});

openMyriadProLightObserver.check().then(() => {
  document.body.classList.add('myriadpro-light--loaded');
}, () => {
  document.body.classList.remove('myriadpro-light--loaded');
});

let imgLoad = ImagesLoaded('.projects-container', function () {
  let masonry = new Masonry('.projects-container', {
    itemSelector: '.project',
    transitionDuration: 0
  });
  baseline();
  masonry = new Masonry('.projects-container', {
    itemSelector: '.project',
    transitionDuration: 0
  });
});

window.addEventListener('resize', baseline);

function baseline() {
  let masonry = Masonry.data(document.querySelector('.projects-container'));
  let cols;
  if (masonry.cols != cols) {
    cols = masonry.cols;
    let items = masonry.items;
    let itemCount = items.reduce(function (previous, current) {
      return (current.element.dataset.orientation == 1) ? previous + 2 : previous + 1;
    }, 0);

    let removeItems = itemCount - (Math.floor(itemCount / cols) * cols);
    let modules = document.querySelectorAll('article[class*=module-]');
    modules.forEach(function (item, index) {
      let count = ++index;
      if (count > (modules.length - removeItems)) {
        item.style.display = 'none';
      } else {
        item.style.display = 'block';
      }
    });
  }
}


const openButton  = document.getElementsByClassName('main__button--show')[0];
const closeButton = document.getElementsByClassName('main__button--close')[0];

const isSafari = /^((?!chrome|android).)*safari/i.test(navigator.userAgent);

const toggleMenu = function () {
  let menuClass  = 'menu-open';
  let classNames = document.body.className.split(' ');

  if (classNames.indexOf(menuClass) !== -1) {
    document.body.className = null;
    let filteredClasses = classNames.filter(item => item !== menuClass);
    document.body.className = filteredClasses.join(' ');
  } else {
    document.body.className += ` ${menuClass}`;
  }
  if (isSafari) {
    setTimeout(function repaintLayout() {
      document.body.style.display = 'none';
      document.body.offsetHeight;
      document.body.style.display = '';
    }, 150);
  }
}

openButton.addEventListener('click', toggleMenu);
closeButton.addEventListener('click', toggleMenu);

const projects = document.getElementsByClassName('project');
for (let i = 0, length = projects.length; i < length; i++) {
  projects[i].addEventListener('touchstart', function (event) {
    console.log(event);
  });
}

import './accordion';

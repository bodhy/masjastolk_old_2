<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Image;
use App\Client;
use App\ImageClient;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(Client $client, Image $image)
    {
      $this->client = $client;
      $this->image = $image;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clients = $this->client->all()->sortBy('position');
        return view('admin.main', compact('clients'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.main');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $this->validate($request, [
        'name' => 'required|unique:clients'
      ]);

      $this->client->create($request->all());

      return redirect()->route('admin::client.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $client = $this->client->find($id);

      $images = $client->images()->with('clientSelectedImage')->get();

      $clientSelectedImages = $images->filter(function($item) {
        return $item->clientSelectedImage;
      })->sortBy(function ($item) {
        return $item->clientSelectedImage->position;
      });

      $images = $images->filter(function ($item) {
        return !$item->clientSelectedImage;
      });

      return view('admin.main', compact('clientSelectedImages', 'client', 'images'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $client = $this->client->find($id);
      return view('admin.main', compact('client'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $this->validate($request, [
        'name' => 'required|unique:clients'
      ]);

      $this->client->find($id)->update($request->all());
      return redirect()->route('admin::client.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $this->client->find($id)->delete();
      return redirect()->back();
    }

    /**
     * Toggle the status of the resource
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function toggleStatus($id)
    {
      $client = $this->client->find($id);
      $client->active = !$client->active;
      $client->save();

      return redirect()->back();
    }

    public function toggleImage($id)
    {
      $image = $this->image->find($id);

      if (!$image->clientSelectedImage) {
        $selected = ImageClient::create([
          'image_id' => $image->id,
          'position' => ImageClient::max('position') + 1
        ]);


        $image->clientSelectedImage()->save($selected);
      } else {
        $image->clientSelectedImage->delete();
      }

      return redirect()->back();
    }

    /**
     * Toggle the status of the resource
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function sort(Request $request) {
      $list = $request->sortedList;

      foreach ($this->client->all() as $client) {
          $client->position = array_search($client->id, $list) + 1;
          $client->save();
      }

      return $list;
    }

    /**
     * Toggle the status of the resource
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function sortImage(Request $request, $id) {
      $client = $this->client->find($id);
      $list = $request->sortedList;

      foreach ($client->images()->has('clientSelectedImage')->get() as $image) {
          $image->clientSelectedImage->update(['position' => array_search($image->clientSelectedImage->id, $list) + 1]);
      }

      return $list;
    }
}

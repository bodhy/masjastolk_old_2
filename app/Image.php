<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'filename', 'size', 'orientation', 'discipline_id', 'client_id'];

    public function getOrientationAttribute($value)
    {
      return ['Landschap', 'Portret'][$value];
    }

    public function projects()
    {
      return $this->belongsToMany(Project::class)->withPivot('id', 'cover', 'cover_position', 'position');
    }

    public function discipline()
    {
      return $this->belongsTo(Discipline::class);
    }

    public function client()
    {
      return $this->belongsTo(Client::class);
    }

    public function clientSelectedImage()
    {
      return $this->hasOne(ImageClient::class);
    }

    public function disciplineSelectedImage()
    {
      return $this->hasOne(ImageDiscipline::class);
    }
}

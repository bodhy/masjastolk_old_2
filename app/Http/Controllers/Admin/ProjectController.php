<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Project;
use App\Discipline;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(Project $project, Discipline $discipline)
    {
      $this->project = $project;
      $this->discipline = $discipline;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projects = $this->project->all()->sortBy('position');
        return view('admin.main', compact('projects'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $disciplines = $this->discipline->all();
        return view('admin.main', compact('disciplines'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $this->validate($request, [
        'name' => 'required|unique:projects',
        'discipline_id' => 'required'
      ]);

      $project = $this->project->create($request->all());
      return redirect()->route('admin::project.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $project = $this->project->find($id);
      return view('admin.main', compact('project'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $project = $this->project->find($id);
      $disciplines = $this->discipline->all();

      return view('admin.main', compact('project', 'disciplines'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $this->validate($request, [
        'name' => 'required|unique:clients',
        'discipline_id' => 'required'
      ]);

      $this->project->find($id)->update($request->all());
      return redirect()->route('admin::project.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $this->project->find($id)->delete();
      return redirect()->back();
    }

    /**
     * Toggle the status of the resource
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function toggleStatus($id)
    {
      $project = $this->project->find($id);
      $project->active = !$project->active;
      $project->save();

      return redirect()->back();
    }

    /**
     * Toggle the status of the resource
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function sort(Request $request, $id) {
      $list = $request->sortedList;

      foreach ($this->project->find($id)->images as $image) {
        $this->project->find($id)->images()->updateExistingPivot($image->id, ['position' => array_search($image->id, $list) + 1]);
      }

      return $list;
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = ['name', 'description'];

  /**
   * Set the user's first name.
   *
   * @param  string  $value
   * @return void
   */
  public function setNameAttribute($value)
  {
      $this->attributes['name'] = $value;
      $this->attributes['slug'] = str_slug($this->attributes['name']);
  }

  public function images()
  {
    return $this->hasMany(Image::class);
  }
}

<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Report;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class ReportController extends Controller
{

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function __construct(Report $report)
  {
    $this->report = $report;
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $reports = $this->report->all()->sortBy('position');
    return view('admin.main', compact('reports'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    return view('admin.main');
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $this->validate($request, [
      'title' => 'required|unique:reports',
      'location' => 'required'
    ]);

    $image = $request->file('image');

    $name = str_replace(['_', ' '], '-', $image->getClientOriginalName());

    $actual_name = pathinfo($name, PATHINFO_FILENAME);
    $original_name = $actual_name;
    $extension = pathinfo($name, PATHINFO_EXTENSION);

    $i = 1;
    while(file_exists(public_path('storage/images/' . $actual_name . '.' . $extension))) {           
        $actual_name = (string) $original_name . $i;
        $name = $actual_name . '.' . $extension;
        $i++;
    }

    $report = $this->report->create([
      'title' => $request->title,
      'location' => $request->location,
      'description' => $request->description,
      'filename' => $name,
    ]);

    if ($image->isValid()) {
      $image = \Intervention::make($request->file('image'));
      $image->backup();

      $image->resize(1000, null, function ($constraint) {
        $constraint->aspectRatio();
        $constraint->upsize();
      })->save(storage_path(sprintf('app/public/images/large/%s', $report->filename)));

      $image->reset();
      $image->backup();

      $image->resize(800, null, function ($constraint) {
        $constraint->aspectRatio();
        $constraint->upsize();
      })->save(storage_path(sprintf('app/public/images/medium/%s', $report->filename)));

      $image->reset();
      $image->backup();

      $image->resize(600, null, function ($constraint) {
        $constraint->aspectRatio();
        $constraint->upsize();
      })->save(storage_path(sprintf('app/public/images/small/%s', $report->filename)));

      $image->reset();

      $image->save(storage_path(sprintf('app/public/images/%s', $report->filename)));
    }

    return redirect()->route('admin::report.index');
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    $report = $this->report->find($id);
    return view('admin.main', compact('report'));  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $this->validate($request, [
      'title' => 'required|unique:reports',
      'location' => 'required'
    ]);
    
    $report = $this->report->find($id)->update([
      'title' => $request->title,
      'location' => $request->location,
      'description' => $request->description,
      'filename' => $request->hasFile('image') ? str_replace(['_', ' '], '-', $image->getClientOriginalName()) : $this->report->find($id)->filename,
    ]);

    if ($request->hasFile('image') && $image->isValid()) {
      $image = \Intervention::make($request->file('image'));
      $image->backup();

      $image->resize(1000, null, function ($constraint) {
        $constraint->aspectRatio();
        $constraint->upsize();
      })->save(storage_path(sprintf('app/public/images/large/%s', $report->filename)));

      $image->reset();
      $image->backup();

      $image->resize(800, null, function ($constraint) {
        $constraint->aspectRatio();
        $constraint->upsize();
      })->save(storage_path(sprintf('app/public/images/medium/%s', $report->filename)));

      $image->reset();
      $image->backup();

      $image->resize(600, null, function ($constraint) {
        $constraint->aspectRatio();
        $constraint->upsize();
      })->save(storage_path(sprintf('app/public/images/small/%s', $report->filename)));

      $image->reset();

      $image->save(storage_path(sprintf('app/public/images/%s', $report->filename)));
    }

    return redirect()->route('admin::report.index');
  }

  /**
   * Toggle the status of the resource
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function toggleStatus($id)
  {
    $report = $this->report->find($id);
    $report->active = !$report->active;
    $report->save();

    return redirect()->back();
  }

  /**
   * Toggle the status of the resource
   *
   * @param  \Illuminate\Http\Request  $request
   */
  public function sort(Request $request) {
    $list = $request->sortedList;

    foreach ($this->report->all() as $report) {
        $report->position = array_search($report->id, $list) + 1;
        $report->save();
    }

    return $list;
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    $report = $this->report->find($id);

    \File::delete(storage_path(sprintf('app/public/images/%s', $report->filename)));
    \File::delete(storage_path(sprintf('app/public/images/small/%s', $report->filename)));
    \File::delete(storage_path(sprintf('app/public/images/medium/%s', $report->filename)));
    \File::delete(storage_path(sprintf('app/public/images/large/%s', $report->filename)));

    $report->delete();
    return redirect()->back();
  }
}

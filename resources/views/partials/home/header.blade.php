<header class="header">
  <div class="header__container">
    <button class="main__button main__button--show"></button>
    <h1 class="header__heading">Masja Stolk Fotografie</h1>
    <p class="header__description">Hallo, mijn naam is Masja Stolk en ik ben een gedreven fotograaf uit hartje Gouda. Mijn werk bestaat voornamelijk uit reportage- en reclamefotografie, waarbij ik me heb gespecialiseerd in portretfotografie. Sinds mijn studie aan de Fotovakschool in Rotterdam is mijn werk niet helemaal onopgemerkt gebleven. Naast mooi werk voor reclamebureau’s, bedrijven en particulieren ben ik regelmatig op reportage in het buitenland.</p>
  </div>
  <nav class="menu">
    <button class="main__button main__button--close"></button>
    <ul class="nav">
      <li class="nav__list-item"><a href="{{ route('biography') }}" class="list-item__anchor">Biografie</a>
      <li class="nav__list-item"><a href="{{ route('reports') }}" class="list-item__anchor">Reportages</a>
      <li class="nav__list-item"><a href="{{ route('top-10') }}" class="list-item__anchor">Top 10</a>
      <li class="nav__list-item" style="display:none;"><a href="{{ route('blog') }}" class="list-item__anchor">Blog</a>
      <li class="nav__list-item"><a href="{{ route('contact') }}" class="list-item__anchor">Contact</a>
    </ul>
  </nav>
</header>

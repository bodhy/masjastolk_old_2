<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Project;
use App\Image;
use App\Client;
use App\Discipline;
use App\ImageProject;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class ImageController extends Controller
{

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function __construct(Project $project, Image $image, Client $client, Discipline $discipline)
  {
    $this->project = $project;
    $this->discipline = $discipline;
    $this->client = $client;
    $this->image = $image;
  }

  public function cover()
  {
    $covers = Project::where('active', true)->get();

    $covers = $covers->map(function($item, $key) {
        return $item->images()->where('cover', true)->get();
    })->reject(function($item) {
        return !$item->count();
    })->collapse()->sortBy(function($item) {
      return $item->pivot->cover_position;
    });

    return view('admin.main', compact('covers'));
  }
  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create($id)
  {
      $project = $this->project->find($id);
      $disciplines = $this->discipline->all();
      $clients = $this->client->all();

      return view('admin.main', compact('project', 'disciplines', 'clients'));
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request, $id)
  {
    $this->validate($request, [
      'discipline_id' => 'required',
      'client_id' => 'required',
      'images.*' => 'image'
    ]);

    $project = $this->project->find($id);

    $discipline = $request->discipline_id;
    $client = $request->client_id;

    foreach ($request->file('images') as $image) {
      if ($image->isValid()) {

        list($origWidth, $origHeight) = getimagesize($image);

        if ($origWidth > $origHeight) {
          $orientation = 0;
        } else {
          $orientation = 1;
        }

        $filename = $this->checkImage(str_replace(['_', ' '], '-', $image->getClientOriginalName()));

        $newFile = $project->images()->create([
          'name' => $image->getClientOriginalName(),
          'filename' => $filename,
          'size' => $image->getClientSize(),
          'orientation' => $orientation,
          'discipline_id' => $discipline,
          'client_id' => $client
        ]);

        $image = \Intervention::make($image);
        $image->backup();

        $image->resize(1000, null, function ($constraint) {
          $constraint->aspectRatio();
          $constraint->upsize();
        })->save(storage_path(sprintf('app/public/images/large/%s', $newFile->filename)));

        $image->reset();
        $image->backup();

        $image->resize(800, null, function ($constraint) {
          $constraint->aspectRatio();
          $constraint->upsize();
        })->save(storage_path(sprintf('app/public/images/medium/%s', $newFile->filename)));

        $image->reset();
        $image->backup();

        $image->resize(600, null, function ($constraint) {
          $constraint->aspectRatio();
          $constraint->upsize();
        })->save(storage_path(sprintf('app/public/images/small/%s', $newFile->filename)));

        $image->reset();

        $image->save(storage_path(sprintf('app/public/images/%s', $newFile->filename)));


        $project->images()->updateExistingPivot($newFile->id, ['position' => $project->images()->max('position') + 1]);
      }
    }

    return redirect()->route('admin::project.show', $project->id);
  }

  public function checkImage($name)
  {
      $actual_name = pathinfo($name, PATHINFO_FILENAME);
      $original_name = $actual_name;
      $extension = pathinfo($name, PATHINFO_EXTENSION);

      $i = 1;
      while(file_exists(public_path('storage/images/' . $actual_name . '.' . $extension))) {           
          $actual_name = (string) $original_name . $i;
          $name = $actual_name . '.' . $extension;
          $i++;
      }
      return $name;
  }

  public function images($projectId)
  {
      $project = $this->project->find($projectId);
      $disciplines = $this->discipline->all();
      $clients = $this->client->all();

      return view('admin.main', compact('project', 'disciplines', 'clients'));
  }

  public function selectedImages($projectId, $disciplineId, $clientId)
  {
    $project = $this->project->find($projectId);
    $images = $this->image->where('discipline_id', $disciplineId)->where('client_id', $clientId)->get();
    $existingImages = $project->images->pluck('id')->toArray();
    $images = $images->filter(function ($item) use ($existingImages) {
      return !in_array($item->id, $existingImages);
    });
    return $images;
  }

  public function assignImage(Request $request, $projectId)
  {
    $project = $this->project->find($projectId);
    $project->images()->attach($request->images);
    return redirect()->route('admin::project.show', $project->id);
  }

  public function toggleCover($projectId, $imageId)
  {

    $project = $this->project->find($projectId);
    $pivot = ImageProject::find($imageId);

    $pivot->cover = !$pivot->cover;
    $pivot->cover_position = $pivot->cover ? ImageProject::all()->where('cover', true)->count() + 1 : 0;
    $pivot->save();


    return redirect()->back();
  }

  /**
   * Toggle the status of the resource
   *
   * @param  \Illuminate\Http\Request  $request
   */
  public function sortCover(Request $request) {
    $list = $request->sortedList;

    $covers = Project::all();

    $covers = $covers->map(function($item, $key) {
        return $item->images()->where('cover', true)->get();
    })->reject(function($item) {
        return !$item->count();
    })->collapse()->sortBy(function($item) {
      return $item->pivot->cover_position;
    });

    foreach ($covers as $cover) {
        $pivot = ImageProject::find($cover->pivot->id);
        $pivot->cover_position = array_search($cover->pivot->id, $list) + 1;
        $pivot->save();
    }

    return $list;
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($project_id, $image_id)
  {
    $this->project->find($project_id)->images()->detach($image_id);
    return redirect()->back();
  }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = ['name', 'description', 'discipline_id'];

  /**
   * Set the user's first name.
   *
   * @param  string  $value
   * @return void
   */
  public function setNameAttribute($value)
  {
      $this->attributes['name'] = $value;
      $this->attributes['slug'] = str_slug($this->attributes['name']);
  }

  public function discipline()
  {
    return $this->belongsTo(Discipline::class);
  }

  public function images()
  {
    return $this->belongsToMany(Image::class)->withPivot('id', 'cover', 'cover_position', 'position');
  }
  public function sortedImages()
  {
    return $this->belongsToMany(Image::class)->withPivot('id', 'cover', 'cover_position', 'position')->orderBy('image_project.position');
  }
}

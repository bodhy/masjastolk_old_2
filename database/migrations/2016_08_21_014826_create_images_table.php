<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('images', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('filename');
            $table->integer('size');
            $table->boolean('orientation');
            $table->integer('discipline_id')->unsigned();
            $table->integer('client_id')->unsigned();
            $table->timestamps();
        });

        Schema::table('images', function (Blueprint $table) {
          $table->foreign('discipline_id')
              ->references('id')
              ->on('disciplines')
              ->onDelete('cascade');

          $table->foreign('client_id')
              ->references('id')
              ->on('clients')
              ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('images');
    }
}

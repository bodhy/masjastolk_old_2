@extends('layouts.master')

@section('title', 'Masja Stolk Photography')

@section('preload')
  <link rel="prefetch" href="{{ route('home') }}">
  <link rel="prerender" href="{{ route('home') }}">
@endsection

@section('app')
  <article class="detail__overview">
    <header class="detail__header">
      <p class="detail__discipline">{{ $project->discipline->name }}</p>
      <h1 class="detail__heading">{{ $project->name }}</h1>
    </header>
    <section class="detail__description">
      <p>{{ $project->description }}</p>
      <button class="detail__button--show-more">Lees meer</button>
    </section>
    <section class="detail__photos">
      @if (isset($images) && count($images))
        @foreach($images as $image)
          @if (count($image) > 1)
            <div class="portrait">
                @foreach($image as $key => $item)
                  <img src="{{ url('storage/images/medium', $item->filename) }}"
                    srcset="{{ url('storage/images/small', $item->filename) }} 375w,
                      {{ url('storage/images/medium', $item->filename) }} 480w,
                      {{ url('storage/images/large', $item->filename) }} 768w"
                    data-src="{{ url('storage/images/medium', $item->filename) }}"
                    date-srcset="{{ url('storage/images/small', $item->filename) }} 375w,
                      {{ url('storage/images/medium', $item->filename) }} 480w,
                      {{ url('storage/images/large', $item->filename) }} 768w"
                    alt="{{ $item->name }}"
                    class="lazyload"
                  >
                    @if (key($image) == $key)
                        <div class="devide"></div>
                    @endif
                @endforeach
            </div>
          @else
            <div class="landscape">
              <img src="{{ url('storage/images/medium', $image->filename) }}"
                srcset="{{ url('storage/images/small', $image->filename) }} 375w,
                  {{ url('storage/images/medium', $image->filename) }} 480w,
                  {{ url('storage/images/large', $image->filename) }} 768w"
                data-src="{{ url('storage/images/medium', $image->filename) }}"
                date-srcset="{{ url('storage/images/small', $image->filename) }} 375w,
                  {{ url('storage/images/medium', $image->filename) }} 480w,
                  {{ url('storage/images/large', $image->filename) }} 768w"
                alt="{{ $image->name }}"
                class="lazyload"
              >
            </div>
          @endif
        @endforeach
      @endif
    </section>
    <a class="main__button main__button--close" href="{{ route('home') }}" role="button">Close</a>
  </article>
@endsection

@section('script')
  <script src="{{ asset('js/project.bundle.js') }}"></script>
@endsection

import Classie from 'desandro-classie';

const accordion = document.getElementsByClassName('link-group__title');
const activeClass = 'link-group--active';

for (let i = 0, length = accordion.length; i < length; i++) {
  accordion[i].addEventListener('click', function (event) {
    const currentLink = event.currentTarget.nextElementSibling;
    Classie.toggle(currentLink, activeClass);
    currentLink.scrollIntoView();
  });
}


/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports) {

	'use strict';

	var windowWidth = window.innerWidth;
	var showMoreButtons = document.querySelectorAll('.detail__button--show-more');

	function isShowMoreNeeded() {
	  if (window.innerWidth != windowWidth) {
	    windowWidth = window.innerWidth;

	    var _iteratorNormalCompletion = true;
	    var _didIteratorError = false;
	    var _iteratorError = undefined;

	    try {
	      for (var _iterator = showMoreButtons[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
	        var showMoreButton = _step.value;

	        var content = showMoreButton.parentNode.querySelector('p');

	        var visibleHeight = content.clientHeight;
	        var actualHide = content.scrollHeight - 1;

	        if (actualHide > visibleHeight) {
	          console.log(actualHide + ' greater than ' + visibleHeight);
	          showMoreButton.style.display = 'block';
	        }
	      }
	    } catch (err) {
	      _didIteratorError = true;
	      _iteratorError = err;
	    } finally {
	      try {
	        if (!_iteratorNormalCompletion && _iterator.return) {
	          _iterator.return();
	        }
	      } finally {
	        if (_didIteratorError) {
	          throw _iteratorError;
	        }
	      }
	    }
	  }
	}

	window.addEventListener('load', isShowMoreNeeded);
	window.addEventListener('resize', isShowMoreNeeded);

	var _iteratorNormalCompletion2 = true;
	var _didIteratorError2 = false;
	var _iteratorError2 = undefined;

	try {
	  var _loop = function _loop() {
	    var showMoreButton = _step2.value;

	    showMoreButton.addEventListener('click', function () {
	      var content = showMoreButton.parentNode.querySelector('p');

	      if (content.style.maxHeight == '500px') {
	        content.style.maxHeight = parseFloat(getComputedStyle(content).fontSize) * 5 + 'px';
	      } else {
	        content.style.maxHeight = '500px';
	      }
	      showMoreButton.innerHTML = getShowLinkText(showMoreButton.innerHTML);
	    });
	  };

	  for (var _iterator2 = showMoreButtons[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
	    _loop();
	  }
	} catch (err) {
	  _didIteratorError2 = true;
	  _iteratorError2 = err;
	} finally {
	  try {
	    if (!_iteratorNormalCompletion2 && _iterator2.return) {
	      _iterator2.return();
	    }
	  } finally {
	    if (_didIteratorError2) {
	      throw _iteratorError2;
	    }
	  }
	}

	function getShowLinkText(currentText) {
	  var newText = '';

	  if (currentText.toUpperCase() === 'Lees meer'.toUpperCase()) {
	    newText = 'Lees minder';
	  } else {
	    newText = 'Lees meer';
	  }

	  return newText;
	}

/***/ }
/******/ ]);
<div class="content__container">
  <div class="content-trail">
    <a href="{{ route('admin::dashboard') }}">Dashboard</a><span>></span><a href="{{ route('admin::cover') }}">Covers</a>
  </div>
  <header class="content__header">
    <h1 class="content__header__heading">Covers</h1>
  </header>
  <section class="content">
    <ul class="overview__cover js-sortable">
      @foreach($covers as $cover)
        <li data-id="{{ $cover->pivot->id }}" style="background: url({{ url('storage/images', $cover->filename) }}) center / cover no-repeat;">
      @endforeach
    </ul>
  </section>
</div>

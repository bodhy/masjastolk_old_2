<div class="content__container">
  <div class="content-trail">
    <a href="{{ route('admin::dashboard') }}">Dashboard</a><span>></span><a href="{{ route('admin::project.index') }}">Klanten</a><span>></span><a href="{{ route('admin::project.edit', $project->id) }}">{{ $project->name }}</a>
  </div>
  <header class="content__header">
    <h1 class="content__header__heading">{{ $project->name }}</h1>
  </header>
  <section class="content">
    <form action="{{ route('admin::project.update', $project->id) }}" method="POST">
      {{ method_field('PUT') }}
      {{ csrf_field() }}
      <div class="input__container">
        <label for="name" class="input__label">Naam</label>
        <input type="input" name="name" value="{{ $project->name }}" placeholder="Bijv. Superman" class="input__textfield" id="name">
      </div>
      <div class="input__container">
        <label for="name" class="input__label">Discipline</label>
        <select class="input__select" name="discipline_id">
          @forelse($disciplines as $discipline)
              <option value="{{ $discipline->id }}" @if(old('discipline_id') && old('discipline') === $discipline->id || $discipline->id === $project->discipline->id) selected @endif>{{ $discipline->name }}</option>
          @empty
            <option value="">Geen disciplines</option>
          @endforelse
        </select>
      </div>
      <div class="input__container">
        <label for="description" class="input__label">Beschrijving</label>
        <textarea type="input" name="description" class="input__textarea" id="description">{{ $project->description }}</textarea>
      </div>
      <div class="input__controls">
        <button type="reset" class="input__button--abort">Annuleren</button>
        <button type="submit" class="input__button--submit">Aanpassen</button>
      </div>
    </form>
    @include('admin.partials.notification')
  </section>
</div>

@extends('layouts.master')

@section('stylesheet')
  <link href="{{ asset('css/admin.bundle.css') }}" rel="stylesheet">
@endsection

@section('app')
  <div class="login">
    <section class="login__panel">
      <h1 class="panel__heading">Inloggen</h1>
      <form action={{ action('Auth\LoginController@login') }} method="POST">
        {{ csrf_field() }}
        <input type="email" class="text-field text-field--email" name="email" placeholder="E-mailadres">
        <input type="password" class="text-field text-field--password" name="password" placeholder="Wachtwoord">
        <button type="submit" class="login__button">Inloggen</button>
      </form>
    </section>
  </div>
@endsection

@section('script')
  <script src="{{ asset('js/admin.bundle.js') }}"></script>
@endsection

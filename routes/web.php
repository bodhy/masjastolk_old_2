<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group([
  'as' => 'admin::',
  'middleware' => 'auth',
  'namespace' => 'Admin',
  'prefix' => 'admin'
], function () {
  Route::get('dashboard', ['as' => 'dashboard', 'uses' => 'DashboardController@index']);
  Route::post('report/status/{id}', ['as' => 'report.toggleStatus', 'uses' => 'ReportController@toggleStatus']);
  Route::post('report/sort', ['as' => 'report.sort', 'uses' => 'ReportController@sort']);
  Route::resource('report', 'ReportController');

  Route::get('cover', ['as' => 'cover', 'uses' => 'ImageController@cover']);
  Route::post('cover/sort', ['as' => 'cover.sort', 'uses' => 'ImageController@sortCover']);

  Route::post('project/{id}/sort', ['as' => 'project.sort', 'uses' => 'ProjectController@sort']);
  Route::post('project/status/{id}', ['as' => 'project.toggleStatus', 'uses' => 'ProjectController@toggleStatus']);
  Route::get('project/{project}/discipline/{discipline}/client/{client}/images', ['as' => 'images', 'uses' => 'ImageController@selectedImages']);
  Route::resource('project', 'ProjectController');

  Route::post('project/{project}/image/assign', ['as' => 'project.image.assign', 'uses' => 'ImageController@assignImage']);
  Route::post('project/{project}/image/{image}/cover', ['as' => 'project.image.toggleCover', 'uses' => 'ImageController@toggleCover']);
  Route::get('project/{project}/images', ['as' => 'project.image.images', 'uses' => 'ImageController@images']);
  Route::resource('project.image', 'ImageController');

  
  Route::resource('images', 'ImagesController');


  Route::post('discipline/status/{id}', ['as' => 'discipline.toggleStatus', 'uses' => 'DisciplineController@toggleStatus']);
  Route::post('discipline/sort', ['as' => 'discipline.sort', 'uses' => 'DisciplineController@sort']);
  Route::post('discipline/{id}/sort', ['as' => 'discipline.sortImage', 'uses' => 'DisciplineController@sortImage']);
  Route::post('discipline/image/{id}', ['as' => 'discipline.toggleImage', 'uses' => 'DisciplineController@toggleImage']);
  Route::resource('discipline', 'DisciplineController');

  Route::post('client/status/{id}', ['as' => 'client.toggleStatus', 'uses' => 'ClientController@toggleStatus']);
  Route::post('client/sort', ['as' => 'client.sort', 'uses' => 'ClientController@sort']);
  Route::post('client/{id}/sort', ['as' => 'client.sortImage', 'uses' => 'ClientController@sortImage']);
  Route::post('client/image/{id}', ['as' => 'client.toggleImage', 'uses' => 'ClientController@toggleImage']);
  Route::resource('client', 'ClientController');

});

Auth::routes();

Route::get('/', ['as' => 'home', 'uses' => 'HomeController@index']);

Route::get('biografie', ['as' => 'biography', 'uses' => 'HomeController@biography']);
Route::get('reportages', ['as' => 'reports', 'uses' => 'HomeController@reports']);
Route::get('top-10', ['as' => 'top-10', 'uses' => 'HomeController@top10']);
Route::get('blog', ['as' => 'blog', 'uses' => 'HomeController@blog']);
Route::get('contact', ['as' => 'contact', 'uses' => 'HomeController@contact']);


Route::get('reportages', ['as' => 'reports', 'uses' => 'HomeController@reports']);
Route::get('reportages', ['as' => 'reports', 'uses' => 'HomeController@reports']);

Route::get('{slug}', ['as' => 'project', 'uses' => 'HomeController@project']);
Route::get('disciplines/{slug}', ['as' => 'discipline', 'uses' => 'HomeController@discipline']);
Route::get('klanten/{slug}', ['as' => 'client', 'uses' => 'HomeController@client']);

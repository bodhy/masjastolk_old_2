@extends('layouts.master')

@section('title', 'Masja Stolk Photography')

@section('preload')
  <link rel="prefetch" href="{{ route('home') }}">
  <link rel="prerender" href="{{ route('home') }}">
@endsection

@section('app')
  <article class="page">
    <h1 class="page__heading">Mijn top 10</h1>
    <p class="page__paragraph">Gek genoeg krijg ik nog wel eens de vraag wat mijn mooiste foto is. Maar wat is mooi? Veel belangrijker vind ik of de foto voldoet aan de vraag van de klant, de vooraf gemaakte afspraken, en de specifieke wensen. Maar ik heb natuurlijk wel een voorkeur. Zoals mijn eerste foto bijvoorbeeld. Of mijn laatste. Of werk gemaakt op één van mijn verre reizen, een coverfoto voor een magazine, een bekende Nederlander of die met een fijne samenwerking tussen fotografie, styling, art direction en klant.</p>
    <a class="main__button main__button--close" href="{{ route('home') }}" role="button">Close</a>
    <section>
      <div class="landscape">
        <figure>
          <img src="http://masjastolk.dev/storage/images/medium/1-reclame-fotografie-fotoshoot-rotterdam.jpg" srcset="http://masjastolk.dev/storage/images/small/1-reclame-fotografie-fotoshoot-rotterdam.jpg 375w,
        http://masjastolk.dev/storage/images/medium/1-reclame-fotografie-fotoshoot-rotterdam.jpg 480w,
        http://masjastolk.dev/storage/images/large/1-reclame-fotografie-fotoshoot-rotterdam.jpg 768w" data-src="http://masjastolk.dev/storage/images/medium/1-reclame-fotografie-fotoshoot-rotterdam.jpg" date-srcset="http://masjastolk.dev/storage/images/small/1-reclame-fotografie-fotoshoot-rotterdam.jpg 375w,
        http://masjastolk.dev/storage/images/medium/1-reclame-fotografie-fotoshoot-rotterdam.jpg 480w,
        http://masjastolk.dev/storage/images/large/1-reclame-fotografie-fotoshoot-rotterdam.jpg 768w" alt="1-reclame-fotografie-fotoshoot-rotterdam.jpg" class=" lazyloaded">
          <figcaption>Reclamefotografie Fashion Victims Rotterdam</figcaption>
        </figure>
      </div>
      <div class="portrait">
        <figure>
       <img src="http://masjastolk.dev/storage/images/medium/1-reclame-fotografie-kempkes-optiek.jpg" srcset="http://masjastolk.dev/storage/images/small/1-reclame-fotografie-kempkes-optiek.jpg 375w,
        http://masjastolk.dev/storage/images/medium/1-reclame-fotografie-kempkes-optiek.jpg 480w,
        http://masjastolk.dev/storage/images/large/1-reclame-fotografie-kempkes-optiek.jpg 768w" data-src="http://masjastolk.dev/storage/images/medium/1-reclame-fotografie-kempkes-optiek.jpg" date-srcset="http://masjastolk.dev/storage/images/small/1-reclame-fotografie-kempkes-optiek.jpg 375w,
        http://masjastolk.dev/storage/images/medium/1-reclame-fotografie-kempkes-optiek.jpg 480w,
        http://masjastolk.dev/storage/images/large/1-reclame-fotografie-kempkes-optiek.jpg 768w" alt="1-hkliving-fotoshoot-lifestyle-interieur.jpg" class=" lazyloaded">
          <figcaption>Studiofotografie Kempkes Optiek Gouda</figcaption>
        </figure>
        <figure>
       <img src="http://masjastolk.dev/storage/images/medium/3-top-10-fotografie-beste-fotograaf.jpg" srcset="http://masjastolk.dev/storage/images/small/3-top-10-fotografie-beste-fotograaf.jpg 375w,
        http://masjastolk.dev/storage/images/medium/3-top-10-fotografie-beste-fotograaf.jpg 480w,
        http://masjastolk.dev/storage/images/large/3-top-10-fotografie-beste-fotograaf.jpg 768w" data-src="http://masjastolk.dev/storage/images/medium/3-top-10-fotografie-beste-fotograaf.jpg" date-srcset="http://masjastolk.dev/storage/images/small/3-top-10-fotografie-beste-fotograaf.jpg 375w,
        http://masjastolk.dev/storage/images/medium/3-top-10-fotografie-beste-fotograaf.jpg 480w,
        http://masjastolk.dev/storage/images/large/3-top-10-fotografie-beste-fotograaf.jpg 768w" alt="3-top-10-fotografie-beste-fotograaf.jpg" class=" lazyloaded">
          <figcaption>Portretfotografie Bodhy Bik</figcaption>
        </figure>
      </div>
      <div class="landscape">
        <figure>
          <img src="http://masjastolk.dev/storage/images/medium/8-documentaire-fotograaf-war-child.jpg" srcset="http://masjastolk.dev/storage/images/small/8-documentaire-fotograaf-war-child.jpg 375w,
        http://masjastolk.dev/storage/images/medium/8-documentaire-fotograaf-war-child.jpg 480w,
        http://masjastolk.dev/storage/images/large/8-documentaire-fotograaf-war-child.jpg 768w" data-src="http://masjastolk.dev/storage/images/medium/8-documentaire-fotograaf-war-child.jpg" date-srcset="http://masjastolk.dev/storage/images/small/8-documentaire-fotograaf-war-child.jpg 375w,
        http://masjastolk.dev/storage/images/medium/8-documentaire-fotograaf-war-child.jpg 480w,
        http://masjastolk.dev/storage/images/large/8-documentaire-fotograaf-war-child.jpg 768w" alt="8-documentaire-fotograaf-war-child.jpg" class=" lazyloaded">
          <figcaption>Reportagefotografie War Child</figcaption>
        </figure>
      </div>
      <div class="landscape">
        <figure>
          <img src="http://masjastolk.dev/storage/images/medium/1-reclame-studio-fotografie-retouche.jpg" srcset="http://masjastolk.dev/storage/images/small/1-reclame-studio-fotografie-retouche.jpg 375w,
        http://masjastolk.dev/storage/images/medium/1-reclame-studio-fotografie-retouche.jpg 480w,
        http://masjastolk.dev/storage/images/large/1-reclame-studio-fotografie-retouche.jpg 768w" data-src="http://masjastolk.dev/storage/images/medium/1-reclame-studio-fotografie-retouche.jpg" date-srcset="http://masjastolk.dev/storage/images/small/1-reclame-studio-fotografie-retouche.jpg 375w,
        http://masjastolk.dev/storage/images/medium/1-reclame-studio-fotografie-retouche.jpg 480w,
        http://masjastolk.dev/storage/images/large/1-reclame-studio-fotografie-retouche.jpg 768w" alt="1-reclame-studio-fotografie-retouche.jpg" class=" lazyloaded">
          <figcaption>Studiofotografie Mobilock</figcaption>
        </figure>
      </div>
      <div class="portrait">
        <figure>
       <img src="http://masjastolk.dev/storage/images/medium/1-portret-profiel-fotograaf-ondernemer.jpg" srcset="http://masjastolk.dev/storage/images/small/1-portret-profiel-fotograaf-ondernemer.jpg 375w,
        http://masjastolk.dev/storage/images/medium/1-portret-profiel-fotograaf-ondernemer.jpg 480w,
        http://masjastolk.dev/storage/images/large/1-portret-profiel-fotograaf-ondernemer.jpg 768w" data-src="http://masjastolk.dev/storage/images/medium/1-portret-profiel-fotograaf-ondernemer.jpg" date-srcset="http://masjastolk.dev/storage/images/small/1-portret-profiel-fotograaf-ondernemer.jpg 375w,
        http://masjastolk.dev/storage/images/medium/1-portret-profiel-fotograaf-ondernemer.jpg 480w,
        http://masjastolk.dev/storage/images/large/1-portret-profiel-fotograaf-ondernemer.jpg 768w" alt="1-hkliving-fotoshoot-lifestyle-interieur.jpg" class=" lazyloaded">
          <figcaption>Portretfotografie De Goudse Verzekeringen</figcaption>
        </figure>
        <figure>
          <img src="http://masjastolk.dev/storage/images/medium/1-hkliving-fotoshoot-lifestyle-interieur.jpg" srcset="http://masjastolk.dev/storage/images/small/1-hkliving-fotoshoot-lifestyle-interieur.jpg 375w,
                  http://masjastolk.dev/storage/images/medium/1-hkliving-fotoshoot-lifestyle-interieur.jpg 480w,
                  http://masjastolk.dev/storage/images/large/1-hkliving-fotoshoot-lifestyle-interieur.jpg 768w" data-src="http://masjastolk.dev/storage/images/medium/1-hkliving-fotoshoot-lifestyle-interieur.jpg" date-srcset="http://masjastolk.dev/storage/images/small/1-hkliving-fotoshoot-lifestyle-interieur.jpg 375w,
                  http://masjastolk.dev/storage/images/medium/1-hkliving-fotoshoot-lifestyle-interieur.jpg 480w,
                  http://masjastolk.dev/storage/images/large/1-hkliving-fotoshoot-lifestyle-interieur.jpg 768w" alt="1-hkliving-fotoshoot-lifestyle-interieur.jpg" class=" lazyloaded">
          <figcaption>Reclamefotografie HK living</figcaption>
        </figure>
      </div>
      <div class="landscape">
        <figure>
          <img src="http://masjastolk.dev/storage/images/medium/7-portret-fotograaf-ngo-locatie1.jpg" srcset="http://masjastolk.dev/storage/images/small/7-portret-fotograaf-ngo-locatie1.jpg 375w,
        http://masjastolk.dev/storage/images/medium/7-portret-fotograaf-ngo-locatie1.jpg 480w,
        http://masjastolk.dev/storage/images/large/7-portret-fotograaf-ngo-locatie1.jpg 768w" data-src="http://masjastolk.dev/storage/images/medium/7-portret-fotograaf-ngo-locatie1.jpg" date-srcset="http://masjastolk.dev/storage/images/small/7-portret-fotograaf-ngo-locatie1.jpg 375w,
        http://masjastolk.dev/storage/images/medium/7-portret-fotograaf-ngo-locatie1.jpg 480w,
        http://masjastolk.dev/storage/images/large/7-portret-fotograaf-ngo-locatie1.jpg 768w" alt="7-portret-fotograaf-ngo-locatie1.jpg" class=" lazyloaded">
          <figcaption>Reportagefotografie Water for Life</figcaption>
        </figure>
      </div>
    </section>
  </article>
@endsection

@section('script')
  <script src="{{ asset('js/project.bundle.js') }}"></script>
@endsection
<div class="content__container">
  <div class="content-trail">
    <a href="{{ route('admin::dashboard') }}">Dashboard</a><span>></span><a href="{{ route('admin::project.index') }}">Projecten</a><span>></span><a href="{{ route('admin::project.edit', $project->id) }}">{{ $project->name }}</a>
  </div>
  <header class="content__header">
    <h1 class="content__header__heading">Afbeeldingen</h1>
    <a href="{{ route('admin::project.image.create', $project->id) }}" class="content__header__add">Afbeelding toevoegen</a>
  </header>
  <section class="content">
    <div class="input__container">
      <label for="discipline_id" class="input__label">Discipline</label>
      <select class="input__select js-discipline" name="discipline_id">
        @forelse($disciplines as $discipline)
            <option value="{{ $discipline->id }}" @if(old('discipline_id') && old('discipline') == $discipline->id) selected @endif>{{ $discipline->name }}</option>
        @empty
          <option value="">Geen disciplines</option>
        @endforelse
      </select>
    </div>
    <div class="input__container">
      <label for="client_id" class="input__label">Klant</label>
      <select class="input__select js-client" name="client_id">
        @forelse($clients as $client)
            <option value="{{ $client->id }}" @if(old('client_id') && old('client') == $client->id) selected @endif>{{ $client->name }}</option>
        @empty
          <option value="">Geen disciplines</option>
        @endforelse
      </select>
    </div>
    <form action="{{ route('admin::project.image.assign', $project->id) }}" method="POST">
      {{ csrf_field() }}
      <select class="images__grid" name="images[]" multiple></select>
      <div class="input__controls">
        <button type="reset" class="input__button--abort">Annuleren</button>
        <button type="submit" class="input__button--submit">Toevoegen</button>
    </div>
    </form>
  </section>
</div>
<script src="http://code.jquery.com/jquery-3.1.1.min.js" integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous"></script>
<script src="{{ url('js/image-picker.min.js') }}"></script>

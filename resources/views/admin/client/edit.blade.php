<div class="content__container">
  <div class="content-trail">
    <a href="{{ route('admin::dashboard') }}">Dashboard</a><span>></span><a href="{{ route('admin::client.index') }}">Klanten</a><span>></span><a href="{{ route('admin::client.edit', $client->id) }}">{{ $client->name }}</a>
  </div>
  <header class="content__header">
    <h1 class="content__header__heading">{{ $client->name }}</h1>
  </header>
  <section class="content">
    <form action="{{ route('admin::client.update', $client->id) }}" method="POST">
      {{ method_field('PUT') }}
      {{ csrf_field() }}
      <div class="input__container">
        <label for="name" class="input__label">Naam</label>
        <input type="input" name="name" value="{{ $client->name }}" placeholder="Bijv. Superman" class="input__textfield" id="name">
      </div>
      <div class="input__container">
        <label for="description" class="input__label">Beschrijving</label>
        <textarea type="input" name="description" class="input__textarea" id="description">{{ $client->description }}</textarea>
      </div>
      <div class="input__controls">
        <button type="reset" class="input__button--abort">Annuleren</button>
        <button type="submit" class="input__button--submit">Aanpassen</button>
      </div>
    </form>
    @include('admin.partials.notification')
  </section>
</div>

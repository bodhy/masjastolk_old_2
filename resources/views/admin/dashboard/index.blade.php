<div class="content__container">
  <div class="content-trail">
    <a href="{{ route('admin::dashboard') }}">Dashboard</a>
  </div>
  <header class="content__header">
    <h1 class="content__header__heading">Dashboard</h1>
  </header>
  <section class="content">
  </section>
</div>

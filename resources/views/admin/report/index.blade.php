<div class="content__container">
  <div class="content-trail">
    <a href="{{ route('admin::dashboard') }}">Dashboard</a><span>></span><a href="{{ route('admin::report.index') }}">Reportages</a>
  </div>
  <header class="content__header">
    <h1 class="content__header__heading">Reportages</h1>
    <a href="{{ route('admin::report.create') }}" class="content__header__add">Toevoegen</a>
  </header>
  <section class="content">
    <table class="content__overview">
      <thead class="overview__header">
        <th>Titel</th>
        <th>Locatie</th>
        <th>Actief</th>
      </thead>
      <tbody class="overview__results js-sortable">
        @foreach($reports as $report)
          <tr data-id="{{ $report->id }}">
            <td onclick="window.location = '{{ route('admin::report.show', $report->id) }}'">{{ $report->title }}</td>
            <td onclick="window.location = '{{ route('admin::report.show', $report->id) }}'">{{ $report->location }}</td>
            <td>
              <form action="{{ route('admin::report.toggleStatus', $report->id) }}" method="POST">
                {{ csrf_field() }}
                <button type="submit" class="result-status">
                  @if ($report->active)
                    <span class="result-status--active">
                  @endif
                </button>
              </form>
            </td>
            <td class="edit-cell"><a href="{{ route('admin::report.edit', $report->id) }}">Aanpassen</a></td>
            <td class="destroy-cell">
              <form action="{{ route('admin::report.destroy', $report->id) }}" method="POST">
                {{ method_field('DELETE') }}
                {{ csrf_field() }}
                <button class="js-warning" type="submit">Verwijderen</button>
              </form>
            </td>
          </tr>
        @endforeach
      </tbody>
    </table>
  </section>
</div>

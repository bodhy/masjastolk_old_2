<footer>
  <section class="links">
    <div class="link-group link-group--contact">
      <ul>
        <li class="link-group__title">Contact
        <li class="link-group__links">
          <ul>
            <li class="link-group__link">Masja Stolk Fotografie
            <li class="link-group__link">Lange Groenendaal 92
            <li class="link-group__link">2801 LV Gouda
            <li class="link-group__link">The Netherlands
            <li class="link-group__link"><a href="tel: 0638779738">Telefoon: 06 38 77 97 38</a>
            <li class="link-group__link"><a href="mailto: info@masjastolk.nl">E-mail: info@masjastolk.nl</a>
            <li class="link-group__button"><a class="social__button social__button--linkedin" href="https://www.linkedin.com/in/masjastolk" target="_blank" rel="noopener noreferrer">LinkedIn</a>
            <li class="link-group__button"><a class="social__button social__button--facebook" href="https://www.facebook.com/masja.stolk" target="_blank" rel="noopener noreferrer">facebook</a>
            <li class="link-group__button"><a class="social__button social__button--instagram" href="https://www.instagram.com/masjastolk/" target="_blank" rel="noopener noreferrer">instagram</a>
          </ul>
      </ul>
    </div>
    <div class="link-group link-group--clients">
      <ul>
        @if ($clients->count())
          <li class="link-group__title">Klanten
          <li class="link-group__links">
            <ul>
              @foreach ($clients as $client)
                <li class="link-group__link">
                  @if ($client->images()->has('clientSelectedImage')->get()->count())
                    <a href="{{ route('client', $client->slug) }}">{{ $client->name }}</a>
                  @else
                    {{ $client->name }}
                  @endif
              @endforeach
            </ul>
        @endif
      </ul>
    </div>
    <div class="link-group link-group--disciplines">
      <ul>
        @if ($disciplines->count())
          <li class="link-group__title">Disciplines
          <li class="link-group__links">
            <ul>
              @foreach ($disciplines as $discipline)
                <li class="link-group__link">
                  @if ($discipline->images()->has('disciplineSelectedImage')->get()->count())
                    <a href="{{ route('discipline', $discipline->slug) }}">{{ $discipline->name }}</a>
                  @else
                    {{ $discipline->name }}
                  @endif
              @endforeach
            </ul>
        @endif
      </ul>
    </div>
  </section>
</footer>

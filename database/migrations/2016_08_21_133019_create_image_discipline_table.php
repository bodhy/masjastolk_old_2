<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImageDisciplineTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('image_discipline', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('image_id')->unsigned();
          $table->integer('position');

          $table->foreign('image_id')
              ->references('id')
              ->on('images')
              ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('image_discipline');
    }
}

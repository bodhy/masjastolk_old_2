@extends('layouts.master')

@section('stylesheet')
  <link href="{{ asset('css/admin.bundle.css') }}" rel="stylesheet">
@endsection

@section('content')
  <div class="login">
    <section class="login__panel">
      <h1 class="panel__heading">Inloggen</h1>
      <form action={{ action('Auth\LoginController@login') }} method="POST">
        {{ csrf_field() }}
        <input type="email" class="text-field text-field--email" name="email" value="{{ old('email') }}" placeholder="E-mailadres">
        <input type="password" class="text-field text-field--password" name="password" placeholder="Wachtwoord">
        <label class="login__remember">Ingelogd blijven<input type="checkbox" name="remember" value="{{ old('remember') }}"></label>
        <a href="{{ action('Auth\ForgotPasswordController@showLinkRequestForm') }}" class="login__recover">Wachtwoord vergeten?</a>
        <button type="submit" class="login__button">Inloggen</button>
      </form>
    </section>
  </div>
@endsection

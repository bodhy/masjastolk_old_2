@extends('layouts.master')

@section('title', 'Masja Stolk Photography')

@section('app')
  <div class="app">
    @include('partials.home.header')
    @include('partials.home.projects')
    @include('partials.home.footer')
  </div>
@endsection

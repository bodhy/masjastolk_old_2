@extends('layouts.master')

@section('title', 'Masja Stolk Photography')

@section('preload')
  <link rel="prefetch" href="{{ route('home') }}">
  <link rel="prerender" href="{{ route('home') }}">
@endsection

@section('app')
  <article class="page">
    @if ($reports->count())
      @foreach($reports as $report)
        <article class="report">
          <header class="detail__header">
            <p class="detail__discipline">{{ $report->location }}</p>
            <h1 class="detail__heading">{{ $report->title }}</h1>
          </header>
          <section class="detail__description">
            <p>{{ $report->description }}</p>
            <button class="detail__button--show-more">Lees meer</button>
          </section>
          <img src="{{ url('storage/images/medium', $report->filename) }}"
            srcset="{{ url('storage/images/small', $report->filename) }} 375w,
              {{ url('storage/images/medium', $report->filename) }} 480w,
              {{ url('storage/images/large', $report->filename) }} 768w"
            data-src="{{ url('storage/images/medium', $report->filename) }}"
            date-srcset="{{ url('storage/images/small', $report->filename) }} 375w,
              {{ url('storage/images/medium', $report->filename) }} 480w,
              {{ url('storage/images/large', $report->filename) }} 768w"
            alt="{{ $report->title }}"
            class="lazyload"
          >
      </article>
    @endforeach
  @endif
  <a class="main__button main__button--close" href="{{ route('home') }}" role="button">Close</a>
</article>
@endsection

@section('script')
  <script src="{{ asset('js/project.bundle.js') }}"></script>
@endsection

<div class="content__container">
  <div class="content-trail">
    <a href="{{ route('admin::dashboard') }}">Dashboard</a><span>></span><a href="{{ route('admin::report.index') }}">Reportages</a>
  </div>
  <header class="content__header">
    <h1 class="content__header__heading">Klanten</h1>
  </header>
  <section class="content">
    <form action="{{ route('admin::report.store') }}" method="POST" enctype="multipart/form-data">
      {{ csrf_field() }}
      <div class="input__container">
        <label for="title" class="input__label">Titel</label>
        <input type="input" name="title" value="{{ old('title') }}" placeholder="Bijv. Superman" class="input__textfield" id="title">
      </div>
      <div class="input__container">
        <label for="location" class="input__label">Locatie</label>
        <input type="input" name="location" value="{{ old('location') }}" placeholder="Bijv. Superman" class="input__textfield" id="location">
      </div>
      <div class="input__container">
        <label for="description" class="input__label">Beschrijving</label>
        <textarea type="input" name="description" class="input__textarea" id="description">{{ old('description') }}</textarea>
      </div>
      <div class="input__container">
        <label for="image" class="input__label">Afbeelding</label>
        <input type="file" name="image" class="input__file" id="image">
      </div>
      <div class="input__controls">
        <button type="reset" class="input__button--abort">Annuleren</button>
        <button type="submit" class="input__button--submit">Toevoegen</button>
      </div>
    </form>
    @include('admin.partials.notification')
  </section>
</div>

<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Image;
use App\Discipline;
use App\ImageDiscipline;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class DisciplineController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function __construct(Discipline $discipline, Image $image)
  {
    $this->discipline = $discipline;
    $this->image = $image;
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
      $disciplines = $this->discipline->all()->sortBy('position');
      return view('admin.main', compact('disciplines'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
      return view('admin.main');
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $this->validate($request, [
      'name' => 'required|unique:clients'
    ]);

    $this->discipline->create($request->all());

    return redirect()->route('admin::discipline.index');
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    $discipline = $this->discipline->find($id);

    $images = $discipline->images()->with('disciplineSelectedImage')->get();

    $disciplineSelectedImages = $images->filter(function($item) {
      return $item->disciplineSelectedImage;
    })->sortBy(function ($item) {
      return $item->disciplineSelectedImage->position;
    });

    $images = $images->filter(function ($item) {
      return !$item->disciplineSelectedImage;
    });

    return view('admin.main', compact('disciplineSelectedImages', 'discipline', 'images'));
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    $discipline = $this->discipline->find($id);
    return view('admin.main', compact('discipline'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $this->validate($request, [
      'name' => 'required|unique:clients'
    ]);

    $this->discipline->find($id)->update($request->all());
    return redirect()->route('admin::discipline.index');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    $this->discipline->find($id)->delete();
    return redirect()->back();
  }

  /**
   * Toggle the status of the resource
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function toggleStatus($id)
  {
    $discipline = $this->discipline->find($id);
    $discipline->active = !$discipline->active;
    $discipline->save();

    return redirect()->back();
  }

  public function toggleImage($id)
  {
    $image = $this->image->find($id);

    if (!$image->disciplineSelectedImage) {
      $selected = ImageDiscipline::create([
        'image_id' => $image->id,
        'position' => ImageDiscipline::max('position') + 1
      ]);


      $image->disciplineSelectedImage()->save($selected);
    } else {
      $image->disciplineSelectedImage->delete();
    }

    return redirect()->back();
  }

  /**
   * Toggle the status of the resource
   *
   * @param  \Illuminate\Http\Request  $request
   */
  public function sort(Request $request) {
    $list = $request->sortedList;

    foreach ($this->discipline->all() as $discipline) {
        $discipline->position = array_search($discipline->id, $list) + 1;
        $discipline->save();
    }

    return $list;
  }

  /**
   * Toggle the status of the resource
   *
   * @param  \Illuminate\Http\Request  $request
   */
  public function sortImage(Request $request, $id) {
    $discipline = $this->discipline->find($id);
    $list = $request->sortedList;

    foreach ($discipline->images()->has('disciplineSelectedImage')->get() as $discipline) {
        $discipline->disciplineSelectedImage->update(['position' => array_search($discipline->disciplineSelectedImage->id, $list) + 1]);
    }

    return $list;
  }
}
